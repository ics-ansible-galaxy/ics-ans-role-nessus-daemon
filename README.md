# ics-ans-role-nessus-daemon

Ansible role to install nessus-daemon.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-nessus-daemon
```

## License

BSD 2-clause
